local myAutoGroup = vim.api.nvim_create_augroup("myAutoGroup", {
	clear = true,
})

local autocmd = vim.api.nvim_create_autocmd

-- 进入Terminal 自动进入插入模式
-- autocmd("TermOpen", {
--   group = myAutoGroup,
--   command = "startinsert",
-- })

-- 保存时自动格式化
autocmd("BufWritePre", {
	group = myAutoGroup,
	pattern = { "*.lua", "*.py", "*.sh" },
	callback = function()
		vim.lsp.buf.format()
	end,
})

-- Highlight on yank
autocmd("TextYankPost", {
	callback = function()
		vim.highlight.on_yank()
	end,
	group = myAutoGroup,
	pattern = "*",
})

-- 用o换行不要延续注释
autocmd("BufEnter", {
	group = myAutoGroup,
	pattern = "*",
	callback = function()
		vim.opt.formatoptions = vim.opt.formatoptions
			- "o" -- O and o, don't continue comments
			+ "r" -- But do continue when pressing enter.
	end,
})

-- 保存Fold
local saveable_type = { "*.lua", "*.js", "*.jsx", "*.ts", "*.tsx" }

autocmd("BufWinEnter", {
	group = myAutoGroup,
	pattern = saveable_type,
	command = "silent! loadview",
})
autocmd("BufWrite", {
	group = myAutoGroup,
	pattern = saveable_type,
	command = "mkview",
})

-- 悬浮窗显示diagnostic
vim.cmd([[
autocmd CursorHold,CursorHoldI * lua vim.diagnostic.open_float(nil, {focus=false})
autocmd QuickFixCmdPost [^l]* nested cwindow
autocmd QuickFixCmdPost    l* nested lwindow
]])
