return {
  "nvim-tree/nvim-tree.lua",
  dependencies = "nvim-tree/nvim-web-devicons",
  config = function()
    local nvim_tree = require("nvim-tree")
    nvim_tree.setup({
      -- open_on_setup = true,
      -- 完全禁止内置netrw
      disable_netrw = true,

      -- 不显示 git 状态图标
      git = {
        enable = true,
        ignore = true,
      },
      update_cwd = false,
      update_focused_file = {
        enable = true,
        update_cwd = false,
      },
      filters = {
        -- 隐藏 .文件
        dotfiles = true,
        -- 隐藏 node_modules 文件夹
        -- custom = { "node_modules" },
      },
      view = {
        -- 宽度
        width = 34,
        -- 也可以 'right'
        side = "left",
        -- 不显示行数
        number = false,
        relativenumber = false,
        -- 显示图标
        signcolumn = "yes",
      },
      actions = {
        open_file = {
          -- 首次打开大小适配
          resize_window = true,
          -- 打开文件时关闭 tree
          quit_on_open = false,
        },
      },
      -- wsl install -g wsl-open
      -- https://github.com/4U6U57/wsl-open/
      system_open = {
        -- mac
        cmd = "open",
        -- windows
        -- cmd = "wsl-open",
      },
      renderer = {
        -- 隐藏根目录
        root_folder_label = false,
        indent_markers = {
          enable = false,
          icons = {
            corner = "└ ",
            edge = "│ ",
            none = "  ",
          },
        },
        icons = {
          webdev_colors = true,
          git_placement = "after",
        },
      },
    })

    -- set keymaps
    local keymap = vim
        .keymap                                                                                                         -- for conciseness

    keymap.set("n", "<A-m>", "<cmd>NvimTreeToggle<CR>", { desc = "Toggle file explorer" })                              -- toggle file explorer
    keymap.set("n", "<leader>ef", "<cmd>NvimTreeFindFileToggle<CR>", { desc = "Toggle file explorer on current file" }) -- toggle file explorer on current file
    keymap.set("n", "<leader>ec", "<cmd>NvimTreeCollapse<CR>", { desc = "Collapse file explorer" })                     -- collapse file explorer
    keymap.set("n", "<leader>er", "<cmd>NvimTreeRefresh<CR>", { desc = "Refresh file explorer" })                       -- refresh file explorer
  end
}
