return {
  "williamboman/mason.nvim",
  dependencies = {
    { "williamboman/mason-lspconfig.nvim" },
    { "WhoIsSethDaniel/mason-tool-installer.nvim" },
    { "neovim/nvim-lspconfig" }
  },
  config = function()
    mason = require("mason")
    mason_config = require("mason-lspconfig")
    mason_tool_installer = require("mason-tool-installer")

    -- :h mason-default-settings
    -- ~/.local/share/nvim/mason
    mason.setup({
      ui = {
        icons = {
          package_installed = "✓",
          package_pending = "➜",
          package_uninstalled = "✗",
        },
      },
    })

    -- mason-lspconfig uses the `lspconfig` server names in the APIs it exposes - not `mason.nvim` package names
    -- https://github.com/williamboman/mason-lspconfig.nvim/blob/main/doc/server-mapping.md
    -- mason的lsp要下载的APP
    mason_config.setup({
      ensure_installed = {
        "clangd",
        -- "jedi_language_server",
        -- "bashls",
        -- "lua_ls",
        -- -- DAP use this
        -- -- cpp Debug: not use cpptools, because cannot attach pid
        -- -- "dockerls",
        -- -- "jsonls",
        "pyright",
      },
    })

    -- mason_tool_installer.setup({
    --     ensure_installed = {
    --         "prettier", -- prettier formatter
    --         "stylua",   -- lua formatter
    --         "isort",    -- python formatter
    --         "black",    -- python formatter
    --         "pylint",
    --         "codelldb", -- use config in 'lua/dap/nvim-dap/config/cpp.lua'
    --         'debugpy',
    --         "cpptools",
    --         "clang-format",
    --         "cmake-language-server",
    --     },
    --     -- run_on_start = true,
    --     -- start_delay = 3000,
    -- })
  end
}
