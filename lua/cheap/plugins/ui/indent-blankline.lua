return {
    -- indent-blankline

    "lukas-reineke/indent-blankline.nvim",
    config = function()
        indent_blankline = require("ibl")
        indent_blankline.setup({
            scope = {
                -- 用 treesitter 判断上下文
                enabled = true,
                show_start = false,
                show_end = false,
                -- v3之后 用默认的highlight 和 exclude
                -- highlight = {
                -- "class",
                -- "function",
                -- "method",
                -- "element",
                -- "^if",
                -- "^while",
                -- "^for",
                -- "^object",
                -- "^table",
                -- "block",
                -- "arguments",
                -- },
                -- -- echo &filetype
                -- exclude = {
                -- "null-ls-info",
                -- "dashboard",
                -- "packer",
                -- "terminal",
                -- "help",
                -- "log",
                -- "markdown",
                -- "TelescopePrompt",
                -- "lsp-installer",
                -- "lspinfo",
                -- "toggleterm",
                -- "text",
                -- },
            },
            indent = {
                -- 竖线样式
                -- char = '¦'
                -- char = '┆'
                -- char = '│'
                -- char = "⎸",

                char = "▏",
            }
        })
    end

}
