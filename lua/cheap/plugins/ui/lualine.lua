return {
	"nvim-lualine/lualine.nvim",
	dependencies = {
		{ "nvim-tree/nvim-web-devicons" },
		{ "folke/tokyonight.nvim" },
	},
	config = function()
		local lualine = require("lualine")
		local theme = require("lualine.themes.tokyonight")
		theme.normal.c.bg = nil

		-- configure lualine with modified theme
		lualine.setup({
			options = {
				-- 指定皮肤
				-- https://github.com/nvim-lualine/lualine.nvim/blob/master/THEMES.md
				theme = theme,
				-- https://github.com/ryanoasis/powerline-extra-symbols
				section_separators = {
					left = " ",
					right = "",
				},
				globalstatus = true,
			},
			extensions = { "nvim-tree" },
			sections = {
				lualine_b = { "branch" },
				lualine_c = {
					{
						"filename",
						file_status = true, -- displays file status (readonly status, modified status)
						path = 2, -- 0 = just filename, 1 = relative path, 2 = absolute path
					},
					--   {
					-- ---  arkav/lualine-lsp-progress
					--     "lsp_progress",
					--     spinner_symbols = { " ", " ", " ", " ", " ", " " },
					--   },
				},
				lualine_x = {
					"diagnostics",
					"filesize",
					{
						"fileformat",
						-- symbols = {
						--   unix = '', -- e712
						--   dos = '', -- e70f
						--   mac = '', -- e711
						-- },
						symbols = {
							unix = "LF",
							dos = "CRLF",
							mac = "CR",
						},
					},
					"encoding",
					"filetype",
				},
			},
		})
	end,
}
