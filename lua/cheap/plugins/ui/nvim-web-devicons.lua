return {
    "kyazdani42/nvim-web-devicons",
    config = function()
        require("nvim-web-devicons").setup {
            override = {
                cuda = {
                    icon = "",
                    -- icon= "",
                    color = "#019833",
                    cterm_color = "65",
                    name = "cuda"
                }
            }
        }
    end,
}
