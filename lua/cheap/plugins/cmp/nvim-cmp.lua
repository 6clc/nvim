return {
    "hrsh7th/nvim-cmp",
    dependencies = { { "L3MON4D3/LuaSnip" }, { "onsails/lspkind-nvim" } },
    config = function()
        -- https://github.com/neovim/nvim-lspconfig/wiki/Autocompletion
        -- https://github.com/hrsh7th/nvim-cmp
        -- https://github.com/onsails/lspkind-nvim
        cmp = require("cmp")
        luasnip = require("luasnip")
        config = require("cheap.uConfig")


        local mapping = {
            -- 出现补全
            [config.keys.cmp_complete] = cmp.mapping(cmp.mapping.complete(), { "i", "c" }),
            -- 取消
            [config.keys.cmp_abort] = cmp.mapping({
                i = cmp.mapping.abort(),
                c = cmp.mapping.close(),
            }),

            -- 确认
            -- Accept currently selected item. If none selected, `select` first item.
            -- Set `select` to `false` to only confirm explicitly selected items.
            [config.keys.cmp_confirm] = cmp.mapping.confirm({
                select = false,
                behavior = cmp.ConfirmBehavior.Replace,
            }),
            -- 如果窗口内容太多，可以滚动
            [config.keys.cmp_scroll_doc_up] = cmp.mapping(cmp.mapping.scroll_docs(-4), { "i", "c" }),
            [config.keys.cmp_scroll_doc_down] = cmp.mapping(cmp.mapping.scroll_docs(4), { "i", "c" }),

            -- 上一个
            [config.keys.cmp_select_prev_item] = cmp.mapping.select_prev_item(),
            -- 下一个
            [config.keys.cmp_select_next_item] = cmp.mapping.select_next_item(),
        }

        cmp.setup({
            -- 指定 snippet 引擎 luasnip
            snippet = {
                expand = function(args)
                    luasnip.lsp_expand(args.body)
                end,
            },
            window = {
                completion = cmp.config.window.bordered(),
                -- documentation = cmp.config.window.bordered(),
            },
            -- 快捷键
            -- mapping = mapping,
            mapping = mapping,
            -- 来源
            sources = cmp.config.sources({
                {
                    name = "luasnip",
                    group_index = 1,
                },
                {
                    name = "nvim_lsp",
                    group_index = 1,
                },
                {
                    name = "nvim_lsp_signature_help",
                    group_index = 1,
                },
                {
                    name = "buffer",
                    group_index = 2,
                },
                {
                    name = "path",
                    group_index = 2,
                },
            }),

            -- 使用lspkind-nvim显示类型图标
            formatting = {
                format = require("lspkind").cmp_format({
                    mode = "symbol_text",
                    -- mode = 'symbol', -- show only symbol annotations
                    maxwidth = 50, -- prevent the popup from showing more than provided characters (e.g 50 will not show more than 50 characters)
                    -- The function below will be called before any actual modifications from lspkind
                    -- so that you can provide more controls on popup customization. (See [#30](https://github.com/onsails/lspkind-nvim/pull/30))
                    before = function(entry, vim_item)
                        -- Source 显示提示来源
                        vim_item.menu = "[" .. string.upper(entry.source.name) .. "]"
                        return vim_item
                    end,
                }),
            }

        })

        -- Use buffer source for `/`.
        cmp.setup.cmdline("/", {
            mapping = cmp.mapping.preset.cmdline(),
            sources = { {
                name = "buffer",
            } },
        })

        -- Use cmdline & path source for ':'.
        cmp.setup.cmdline(":", {
            mapping = cmp.mapping.preset.cmdline(),
            sources = cmp.config.sources({ {
                name = "path",
            } }, { {
                name = "cmdline",
            } }),
        })

        cmp.setup.filetype({ "markdown", "help" }, {
            sources = { {
                name = "luasnip",
            }, {
                name = "buffer",
            }, {
                name = "path",
            } },
        })
    end
}
