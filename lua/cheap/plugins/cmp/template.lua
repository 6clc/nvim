return {
    'nvimdev/template.nvim',
    dependencies = { "nvim-telescope/telescope.nvim" },
    cmd = { 'Template', "TemProject" },
    config = function()
        require('template').setup({
            temp_dir = vim.fn.stdpath('config') .. '/template',
            author = '6clc',
            email = 'chaoliu.lc@qq.com'
        })
        require("telescope").load_extension('find_template')
    end,
}
