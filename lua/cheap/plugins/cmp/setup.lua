return {
  -- 补全源
  { "hrsh7th/cmp-vsnip" },
  { "hrsh7th/cmp-nvim-lsp" },                -- { name = nvim_lsp },
  { "hrsh7th/cmp-buffer" },                  -- { name = 'buffer' },,
  { "hrsh7th/cmp-path" },                    -- { name = 'path' },
  { "hrsh7th/cmp-cmdline" },                 -- { name = 'cmdline' },
  { "hrsh7th/cmp-nvim-lsp-signature-help" }, -- { name = 'nvim_lsp_signature_help' },
  { "saadparwaiz1/cmp_luasnip" },
  { "rafamadriz/friendly-snippets" },        -- 常见编程语言代码段
}
