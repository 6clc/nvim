local M = {}

function M.setup()
  local dap = require("dap")
  dap.adapters.codelldb = {
    type = "server",
    port = "${port}",
    executable = {
        -- #hard
        command = "/root/.local/share/nvim/mason/packages/codelldb/codelldb",
        args = { "--port", "${port}" },
    },
  }
  dap.adapters.cppdbg = {
    id = 'cppdbg',
    type = "executable",
    -- change to your path
    -- if can not find OpenDebugAD7, please install cpptools by mason
    command= "/root/.local/share/nvim/mason/packages/cpptools/extension/debugAdapters/bin/OpenDebugAD7",
  }


  dap.configurations.cpp = {
    {
        name = "Launch (codelldb)",
        type = "codelldb",
        request = "launch",
        program = function()
            return vim.fn.input("Path to executable: ", vim.fn.getcwd() .. "/", "file")
        end,
        cwd = "${workspaceFolder}",
        stopOnEntry = false,
        -- runInTerminal = true,
        -- terminal = "integrated",
    },
    {
        name = "attach PID (codelldb)",
        type = "codelldb",
        request = "attach",
        pid = require("dap.utils").pick_process,
    },
    {
        name = "Launch With Argument(codelldb)",
        type = "codelldb",
        request = "launch",
        program = function()
            return vim.fn.input("Path to executable: ", vim.fn.getcwd() .. "/", "file")
        end,
        cwd = "${workspaceFolder}",
        stopOnEntry = false,
        -- runInTerminal = true,
        -- terminal = "integrated",
        args = function()
            local args = {}
            local i = 1
            while true do
                local arg = vim.fn.input("Argument [" .. i .. "]: ")
                if arg == "" then
                    break
                end
                args[i] = arg
                i = i + 1
            end
            return args
        end,
    },
    {
        name = "Attach to Name (wait) (codelldb)",
        type = "codelldb",
        request = "attach",
        program = function()
            return fn.input("Path to executable: ", fn.getcwd() .. "/", "file")
        end,
        waitFor = true,
    },
  }

  -- setup other language
  dap.configurations.c = dap.configurations.cpp

  -- cuda
  dap.configurations.cuda = {
    {
      name = "Launch file",
      type = "cppdbg",
      request = "launch",
      MIMode = "gdb",
      miDebuggerPath = "cuda-gdb",
      -- stopOnEntry = true,
      program = function()
        return vim.fn.input('Path to executable: ', vim.fn.getcwd() .. '/', 'file')
      end,
      cwd = '${workspaceFolder}',
      stopOnEntry = false,
      setupCommands = {
        {
          text = '-enable-pretty-printing',
          description =  'enable pretty printing',
          ignoreFailures = false 
        },
      },
    },
  }
end

return M
