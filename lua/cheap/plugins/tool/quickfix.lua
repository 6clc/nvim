return {
    "6clc/nvim-bqf",
    config = function()
        require('bqf').setup({
            auto_enable = true,
            preview = {
                win_height = 999,
                win_vheight = 999,
                delay_syntax = 80,
                show_scroll_bar = false,
                border = { '┏', '━', '┓', '┃', '┛', '━', '┗', '┃' },
                show_title = false,
                should_preview_cb = function(bufnr, qwinid)
                    local ret = true
                    local bufname = vim.api.nvim_buf_get_name(bufnr)
                    local fsize = vim.fn.getfsize(bufname)
                    if fsize > 100 * 1024 then
                        -- skip file size greater than 100k
                        ret = false
                    elseif bufname:match('^fugitive://') then
                        -- skip fugitive buffer
                        ret = false
                    end
                    return ret
                end
            },
        })
    end,
}
