return {
    'phaazon/hop.nvim',
    config = function()
        hop = require("hop")
        -- https://github.com/phaazon/hop.nvim
        -- https://github.com/phaazon/hop.nvim/wiki
        -- https://github.com/phaazon/hop.nvim/wiki/Configuration
        hop.setup {
            keys = 'etovxqpdygfblzhckisuran',
        }
        uHop = {
            lines = "gl",
            words = "gw",
            char = "gch"
        }
        keymap("n", uHop.lines, ":HopLine<CR>")
        keymap("n", uHop.words, ":HopWord<CR>")
        keymap("n", uHop.char, ":HopChar1<CR>")
    end
}
