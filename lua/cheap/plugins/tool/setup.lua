return {
	{ "airblade/vim-rooter" },
	{ "andrewradev/linediff.vim" },
	{ "mbbill/undotree" },
	{
		"sindrets/diffview.nvim",
		dependencies = { "nvim-lua/plenary.nvim" },
	},
}
