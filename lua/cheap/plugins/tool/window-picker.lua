return {
	"s1n7ax/nvim-window-picker",
	name = "window-picker",
	event = "VeryLazy",
	version = "2.*",
	config = function()
		local picker = require("window-picker")
		picker.setup({
			-- the foreground (text) color of the picker
			fg_color = "#000000",

			-- if you have include_current_win == true, then current_win_hl_color will
			-- be highlighted using this background color
			current_win_hl_color = "#7ab8cc",

			-- all the windows except the curren window will be highlighted using this
			-- color
			other_win_hl_color = "#7ab8cc",
		})

		local uConfig = require("cheap.uConfig")
		vim.keymap.set("n", uConfig.windowPicker.picker, function()
			local picked_window_id = picker.pick_window() or vim.api.nvim_get_current_win()
			vim.api.nvim_set_current_win(picked_window_id)
		end, { desc = "Pick a window" })
	end,
}
