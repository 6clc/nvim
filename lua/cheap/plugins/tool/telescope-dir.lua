
return {
    "princejoogie/dir-telescope.nvim",
    dependencies = {"nvim-telescope/telescope.nvim"},
    config = function()
      require("dir-telescope").setup({
        -- these are the default options set
        hidden = true,
        show_preview = true,
      })
    end,
}


