return {
    "skywind3000/asynctasks.vim",
    dependencies = "skywind3000/asyncrun.vim",
    config = function()
        vim.g.asyncrun_open = 15
        vim.g.asynctasks_extra_config = {
            '~/.config/nvim/config/tasks.ini',
            '~/.vim/config/tasks.ini'
        }
        vim.g.asynctask_template = '~/.config/nvim/config/task_template.ini'
    end,
}
