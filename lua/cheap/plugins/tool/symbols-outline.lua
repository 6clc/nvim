local uConfig = require("cheap.uConfig")
local uSymbols = uConfig.symbols_outline


return {
  "stevearc/aerial.nvim",
  config = function()
    symbols_outline = require("aerial")
    symbols_outline.setup({
      backends = { "treesitter", "lsp" },
      layout = {
        default_direction = "prefer_right",
      }
    })
    keymap("n", uSymbols.toggle, ":AerialToggle!<CR>")
  end
}
