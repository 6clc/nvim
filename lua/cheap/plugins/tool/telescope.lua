return {
	"nvim-telescope/telescope.nvim",
	dependencies = {
		-- telescope extensions
		{ "nvim-lua/plenary.nvim" },
		{ "LinArcX/telescope-env.nvim" },
		{ "nvim-telescope/telescope-ui-select.nvim" },
	},
	config = function()
		local uTelescope = {

			enable = true,

			find_files = "<C-p>p",
			live_grep = "<C-f>",
			mru = "<C-n>",
			mark = "<leader>m",
			dir_find_files = "<leader>pp",
			dir_live_grep = "<leader>ff",

			-- 上下移动
			move_selection_next = "<C-j>",
			move_selection_previous = "<C-k>",
			-- move_selection_next = "<C-n>",
			-- move_selection_previous = "<C-p>",
			-- 历史记录
			cycle_history_next = "<Down>",
			cycle_history_prev = "<Up>",
			-- 关闭窗口
			-- close = "<C-c>",
			close = "<esc>",
			-- 预览窗口上下滚动
			preview_scrolling_up = "<C-u>",
			preview_scrolling_down = "<C-d>",
		}
		local telescope = require("telescope")

		local actions = require("telescope.actions")
		telescope.setup({
			defaults = {
				-- 打开弹窗后进入的初始模式，默认为 insert，也可以是 normal
				initial_mode = "insert",
				-- vertical , center , cursor
				layout_strategy = "horizontal",
				-- path_display = { "smart" },
				-- 窗口内快捷键
				mappings = {
					i = {
						-- 上下移动
						[uTelescope.move_selection_next] = actions.move_selection_next,
						[uTelescope.move_selection_previous] = actions.move_selection_previous,
						-- 历史记录
						[uTelescope.cycle_history_next] = "cycle_history_next",
						[uTelescope.cycle_history_prev] = "cycle_history_prev",
						-- 关闭窗口
						-- ["<esc>"] = actions.close,
						[uTelescope.close] = "close",
						-- 预览窗口上下滚动
						[uTelescope.preview_scrolling_up] = "preview_scrolling_up",
						[uTelescope.preview_scrolling_down] = "preview_scrolling_down",
					},
				},
				-- from https://github.com/nvim-telescope/telescope.nvim/issues/1958#issuecomment-1134720975
				find_files = {
					find_command = { "fd", "-t=f", "-a" },
					path_display = { "absolute" },
					wrap_results = true,
				},
				file_ignore_patterns = {
					"node_modules/",
					".git/",
					"dist/",
					"yarn.lock",
					"package-lock.json",
				},
			},
			pickers = {
				find_files = {
					-- theme = "dropdown", -- 可选参数： dropdown, cursor, ivy
				},
			},
			extensions = {
				["ui-select"] = {
					require("telescope.themes").get_dropdown({
						-- even more opts
						initial_mode = "normal",
					}),
				},
			},
		})

		keymap("n", uTelescope.find_files, ":Telescope find_files<CR>")
		keymap("n", uTelescope.live_grep, ":Telescope live_grep<CR>")
		keymap("n", uTelescope.mru, ":Telescope oldfiles<CR>")
		keymap("n", uTelescope.mark, ":Telescope marks<CR>")
		keymap("n", uTelescope.dir_find_files, ":Telescope dir find_files<CR>")
		keymap("n", uTelescope.dir_live_grep, ":Telescope dir live_grep<CR>")

		pcall(telescope.load_extension, "env")
		-- To get ui-select loaded and working with telescope, you need to call
		-- load_extension, somewhere after setup function:
		pcall(telescope.load_extension, "ui-select")
		pcall(telescope.load_extension, "dir")
	end,
}
