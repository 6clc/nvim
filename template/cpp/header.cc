/**
 * @author      : {{_author_}} {{_email_}}
 * @created     : {{_date_}}
 */


#ifndef {{_lua:string.upper(vim.fn.expand("%:.")):gsub("[/.%-]", "_").."_"_}} 
#define {{_lua:string.upper(vim.fn.expand("%:.")):gsub("[/.%-]", "_").."_"_}}

{{_cursor_}}

#endif /* end of include guard {{_lua:string.upper(vim.fn.expand("%:.")):gsub("[/.%-]", "_").."_"_}}*/
