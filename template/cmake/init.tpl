;; cmake
/**
 * @author      : {{_author_}} {{_email_}}
 * @created     : {{_date_}}
 */

cmake_minimum_required(VERSION 2.8)
project( {{_cursor_}} LANGUAGES  CXX)
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -pthread")

