## auto install tmux and nvim


```bash
bash scripts/docker/install.sh tmux
bash scripts/docker/install.sh all
```
## 安装最新nvim

### ubuntu

```bash
  apt remove -y neovim
  apt install -y ninja-build gettext libtool-bin cmake g++ pkg-config unzip curl
  git clone https://github.com/neovim/neovim.git
  cd neovim
  make -j60 CMAKE_BUILD_TYPE=RelWithDebInfo
  make install
```



## 自检方式

```bash
:checkhealth
```

## leader key

leader key默认为空格键，定义的位置在 

> lua/cheap/keybindings.lua:22-25
```
-- leader key 为空
local leader_key = " "
vim.g.mapleader = leader_key
vim.g.maplocalleader = leader_key
```

## 复制粘贴

只有使用的终端支持osc52协议，才可以和系统共用剪切板。

### CopyWithMetadata

除了支持普通模式的copy外，支持拷贝代码所在的文件和行号， visual mode选择copy的文本，执行`:CopyWithMetadata`

```bash
:'<,'>CopyWithMetadata
```

得到的拷贝结果如下

````bash
> nvim/init.lua:8-11
```
要拷贝的代码段
```
````

## window、tab管理

快捷键定义如下

> nvim/lua/cheap/uConfig.lua:38-77
```
    s_windows = {

      enable = true,

      -- 窗口开关
      split_vertically = "sv",
      split_horizontally = "sh",
      close = "sc",
      close_others = "so",

      -- 窗口跳转
      jump_left = "<A-h>",
      jump_right = "<A-l>",
      jump_up = "<A-k>",
      jump_down = "<A-j>",

      -- 窗口比例控制
      width_decrease = "s,",
      width_increase = "s.",
      height_decrease = "sj",
      height_increase = "sk",
      size_equal = "s=",
    },

    s_tab = {
      split = "ts",
      prev = "th",
      next = "tl",
      first = "tj",
      last = "tk",
      close = "tc",
      close_others = "to",
    },

```

## cmp （自动完成）

相关配置在 `lua/cheap/plugins/cmp/`，借助lsp、snippet、AST分析完成自动提示、补全的功能。不需要特别关注

##  format（格式化）

快捷键 `<leader> f`

## lsp 

### 自动安装lsp

在https://github.com/williamboman/mason-lspconfig.nvim/blob/main/doc/server-mapping.md找打自己要安装语言的lsp，在配置文件中添加对应的名字即可

> lua/cheap/plugins/mason.lua:27-40
```
    -- mason的lsp要下载的APP
    mason_config.setup({
      ensure_installed = {
        "clangd",
        -- "jedi_language_server",
        -- "bashls",
        -- "lua_ls",
        -- -- DAP use this
        -- -- cpp Debug: not use cpptools, because cannot attach pid
        -- -- "dockerls",
        -- -- "jsonls",
        "pyright",
      },
    })
```

### lsp快捷键

1. 可以通过 `:lspsaga <TAB>`来选择所有lsp的命令，比如说`:lspsaga rename`修改变量的名字
2. 预定义的一些快捷键如下

> nvim//home/lc6c/.config/nvim/lua/cheap/plugins/lsp/lspconfig.lua:48-86
```
				-- set keybinds
				opts.desc = "Show LSP references"
				keymap.set("n", "gR", "<cmd>Telescope lsp_references<CR>", opts) -- show definition, references

				opts.desc = "Go to declaration"
				keymap.set("n", "gD", vim.lsp.buf.declaration, opts) -- go to declaration

				opts.desc = "Show LSP definitions"
				keymap.set("n", "gd", "<cmd>Telescope lsp_definitions<CR>", opts) -- show lsp definitions

				opts.desc = "Show LSP implementations"
				keymap.set("n", "gi", "<cmd>Telescope lsp_implementations<CR>", opts) -- show lsp implementations

				opts.desc = "Show LSP type definitions"
				keymap.set("n", "gt", "<cmd>Telescope lsp_type_definitions<CR>", opts) -- show lsp type definitions

				opts.desc = "See available code actions"
				keymap.set({ "n", "v" }, "<leader>ca", vim.lsp.buf.code_action, opts) -- see available code actions, in visual mode will apply to selection

				opts.desc = "Smart rename"
				keymap.set("n", "<leader>rn", vim.lsp.buf.rename, opts) -- smart rename

				opts.desc = "Show buffer diagnostics"
				keymap.set("n", "<leader>D", "<cmd>Telescope diagnostics bufnr=0<CR>", opts) -- show  diagnostics for file

				opts.desc = "Show line diagnostics"
				keymap.set("n", "<leader>d", vim.diagnostic.open_float, opts) -- show diagnostics for line

				opts.desc = "Go to previous diagnostic"
				keymap.set("n", "[d", vim.diagnostic.goto_prev, opts) -- jump to previous diagnostic in buffer

				opts.desc = "Go to next diagnostic"
				keymap.set("n", "]d", vim.diagnostic.goto_next, opts) -- jump to next diagnostic in buffer

				opts.desc = "Show documentation for what is under cursor"
				keymap.set("n", "K", vim.lsp.buf.hover, opts) -- show documentation for what is under cursor

				opts.desc = "Restart LSP"
				keymap.set("n", "<leader>rs", ":LspRestart<CR>", opts) -- mapping to restart lsp if necessary
```

## git管理

`:DiffviewOpen` 查看stash、unstash的文件及其文件内容的修改

`:Gitsigns toggle_current_line_blame` 显示当前行数的author

## DAP： 可视化单步调试

TODO: 待补充

## 效率提升

### 一键执行脚本 AsyncTasks

详细的用法见

- ​    https://github.com/skywind3000/asyncrun.vim/blob/master/README-cn.md  
- ​    https://github.com/skywind3000/asynctasks.vim  

简要的步骤如下

#### 1. 在项目目录执行 `:AsyncTaskEdit` 会自动在项目根目录新建.tasks文件，文件格式如下

```bash
[file-build]  # 任务名字

# shell command, use quotation for filenames containing spaces
# check ":AsyncTaskMacro" to see available macros
command=gcc "$(VIM_FILEPATH)" -o "$(VIM_FILEDIR)/$(VIM_FILENOEXT)" # 任务执行的脚本

# working directory, can change to $(VIM_ROOT) for project root
cwd=$(VIM_FILEDIR)                                                 # 执行脚本前要cd到的目录

# output mode, can be one of quickfix and terminal
# - quickfix: output to quickfix window
# - terminal: run the command in the internal terminal
output=quickfix                                                    # 脚本输出的位置，默认在quickfix,其他的可以看官方文档

# this is for output=quickfix only
# if it is omitted, vim's current errorformat will be used.
errorformat=%f:%l:%m                                               # 正则匹配log中错误日志，匹配上后点击可以直接跳转错误处

# save file before execute
save=1                                                             # 默认为1，执行前会自动保存所有修改

```

#### 2. 执行`:AsyncTask  file-build` 就可以执行上面定义的 `file-build`任务

#### 3. 快捷键， 配置文件中定义了3各任务名字的快捷键，如下

> lua/cheap/keybindings.lua:127-131
```
-- async task
keymap("n", "<F4>", ":AsyncTask file-build<CR>")
keymap("n", "<F5>", ":AsyncTask file-build-debug<CR>")
keymap("n", "<F6>", ":AsyncTask file-run<CR>")
keymap("n", "<F1>", ":AsyncStop <CR>")
```

### 随意跳转 hop

TODO

### 随意选择窗口 window-picker

TODO

### 自动mark

TODO

### 任意文本diff

TODO

