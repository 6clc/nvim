-- 基础配置
require("cheap.basic")

-- 全局函数
require("cheap.utils.global")

-- meta copy
require("cheap.utils.copy_line")

-- 快捷键映射
require("cheap.keybindings")

-- lazy plugins
require("cheap.lazy")

-- 自动命令
require("cheap.utils.autocmds")

-- -- 复制到windows剪贴板
-- -- require('utils.fix-yank')
-- --
--
vim.lsp.set_log_level("off")
