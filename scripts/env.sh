#!/bin/bash
set -x

WIKI_PATH=$(dirname $(readlink -f $0))
echo "当前脚本所在目录的绝对路径: $script_dir"


function move_git() {
	mkdir -p $HOME/git
	mv .git $HOME/git/wiki.git
	echo "gitdir: ../git/wiki.git" >> .git
	pushd $HOME/git/wiki.git
	git config core.worktree $HOME/wiki
}
function dowload_other(){
	if [ -f ~/.vim/tasks.ini ]; then rm -rf  ~/.vim/tasks.ini ; fi
	cp configs/tasks.ini  ~/.vim/tasks.ini

	cp configs/espanso/default.yml ~/.config/espanso/default.yml

}
function upload_other(){
	cp ~/.vim/tasks.ini configs/tasks.ini
	mkdir -p configs/espanso
	cp ~/.config/espanso/default.yml configs/espanso/default.yml
	cp ~/.zshrc configs/.zshrc
}
function init_git() {
	git config --global core.editor "vim"
	git config --global alias.lg "log --color --graph --pretty=format:'%Cred%h%Creset -%C(yellow)%d%Creset %s %Cgreen(%cr) %C(bold blue)<%an>%Creset' --abbrev-commit"
	git config --global alias.co checkout
	git config --global alias.br branch
	git config --global alias.ct commit
	git config --global alias.ss status
	git config --global core.quotepath false
#	git config user.name liuchao
#	git config user.email chaoliu.lc@qq.com
}

function init_vim() {
    mkdir -p ~/.vim
	pushd ~/.vim
	git clone https://gitlab.com/6clc/vim-init.git
	echo "source ~/.vim/vim-init/init.vim" >> ~/.vimrc
	popd
}

function init_nvim(){
	pushd ~/.config
	git clone https://gitlab.com/6clc/nvim.git
	popd
}

function init_tmux() {
	echo "oh my tmux"
	pushd $HOME
	git clone https://github.com/gpakosz/.tmux.git
	ln -s -f .tmux/.tmux.conf
	cp ${WIKI_PATH}/configs/.tmux.conf.local .
	popd

	echo "intsall tmux pac manager"
	git clone https://github.com/tmux-plugins/tpm ~/.tmux/plugins/tpm
	bash ~/.tmux/plugins/tpm/bin/install_plugins
}

function init_zsh () {
	cp ${WIKI_PATH}/configs/.zshrc ~/.
	cp ${WIKI_PATH}/configs/.p10k.zsh ~/.
}


function init(){
	init_git
	init_tmux
	init_zsh
	init_vim
	init_nvim
	# yank in terminal
	# bash scripts/install_yank.sh
}


function download_config() {
	echo "================== async vimrc ============="
	if [ -f ~/.vimrc ]; then rm -rf ~/.vimrc; fi
	cp configs/.vimrc ~/

	echo "=================== async tmux config ========= "
	if [ -f ~/.tmux.conf.local ]; then rm -rf ~/.tmux.conf.local; fi
	cp configs/.tmux.conf.local ~/

	echo "=================== async zsh ======================"
	if [ -f ~/.zshrc ]; then mv ~/.zshrc ~/.zshrc.bak; fi
	cp configs/.zshrc ~/

	echo "==================== async alacritty/alacritty.yml config ================="
	if [ -f ~/.config/alacritty/alacritty.yml ]; then rm -rf ~/.config/alacritty/alacritty.yml; fi
	cp configs/alacritty/alacritty.yml ~/.config/alacritty/

	# echo "===================== async git config ================="
	# if [ -f ~/.gitconfig ]; then rm -rf  ~/.gitconfig; fi
	# cp configs/.gitconfig ~/.gitconfig
    
    cp configs/.globalrc  ~/.globalrc

}

function upload_config() {
	echo "aync vimrc"
	cp ~/.vimrc configs/
	echo "async tmux"
	cp ~/.tmux.conf.local configs/
#	echo "async terminator"
#	cp ~/.config/alacritty/alacritty.yml configs/alacritty/alacritty.yml
#	echo "async git"
#	cp ~/.gitconfig configs/
	echo "async zshrc"
	cp ~/.zshrc configs/
	echo "asynv config"
}

function usage(){
	echo "1. upload: upload setting"
	echo "2. download: download setting"
	echo "3. init: first use"
}

function main() {
	local cmd=$1
	case $cmd in
		download)
			download_config
			;;
		upload)
			upload_config
			;;
		init)
			init
			;;
		move)
			move_git
			;;
		help)
			usage
			;;
		*)
			echo "no thing execute"
			usage
			;;
	esac
}

main $@

