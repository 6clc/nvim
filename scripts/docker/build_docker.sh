if [[ "" == "$1" ]]; then
BASE_IMAGE=nvcr.io/nvidia/tensorrt:21.12-py3
BASE_IMAGE=nvcr.io/nvidia/tensorflow:21.10-tf1-py3
BASE_IMAGE=nvcr.io/nvidia/tensorflow:19.06-py3

else
BASE_IMAGE=$1
fi
# --no-cache \
docker build \
             --tag $BASE_IMAGE"-liuchao" \
             --build-arg BASE_IMAGE=$BASE_IMAGE  \
             --build-arg USERNAME=$USER \
             --build-arg USER_UID=$(id -u) \
             --build-arg USER_GID=$(id -g) \
             --build-arg CONFIG_URL="http://192.168.68.50:9999/config.tar.gz" \
             --file=liuchao.dockerfile \
             .
