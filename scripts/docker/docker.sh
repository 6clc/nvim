MM_IMAGE=nvcr.io/nvidia/tensorrt:22.04-py3
MM_IMAGE=nvcr.io/nvidia/tensorrt:21.12-py3-liuchao
MM_IMAGE=nvcr.io/nvidia/tensorflow:21.10-tf1-py3-liuchao
MM_IMAGE=ubuntu:trusty
MM_IMAGE=nvcr.io/nvidia/tensorflow:19.06-py3
OPT_DIR="/ssd2/liuchao52/opt"

CONTAINER_NAME=transformer_liuchao
CONFIG_URL="http://10.100.131.233:9999/config.tar.gz"

set -x
function run() {

  docker rm -f $CONTAINER_NAME

  docker run -it  \
    --name=$CONTAINER_NAME \
    --gpus=all \
    --net=host \
    --pid=host \
    --ipc=host \
    --privileged=true \
    -v /home/liuchao/prs:/prs \
    -v /home/liuchao/lc:/lc \
    -v /run/dbus/system_bus_socket:/run/dbus/system_bus_socket:ro \
    -w /prs \
    ${MM_IMAGE}
  }

function exec() {
  docker exec -it $CONTAINER_NAME bash
}


function install(){

sed -i 's/archive.ubuntu.com/mirrors.ustc.edu.cn/g' /etc/apt/sources.list
sed -i 's/cn.archive.ubuntu.com/mirrors.ustc.edu.cn/g' /etc/apt/sources.list
sed -i 's/security.ubuntu.com/mirrors.ustc.edu.cn/g' /etc/apt/sources.list
pip3 config set global.index-url https://pypi.tuna.tsinghua.edu.cn/simple

add-apt-repository -y ppa:git-core/ppa
apt-get update
apt-get install -y zsh git



# neovim
mkdir -p /${OPT_DIR} && pushd /${OPT_DIR}
apt remove -y neovim
apt install -y ninja-build gettext libtool-bin cmake g++ pkg-config unzip curl
# rm -rf neovim
git clone https://github.com/neovim/neovim.git
cd neovim
git reset --hard b263c73b0

# command from build in windows at neovim website
mkdir .deps
cd .deps
cmake -G Ninja ../cmake.deps/
ninja
cd ..

mkdir build
cd build
cmake -G Ninja -D CMAKE_BUILD_TYPE=RelWithDebInfo ..
ninja
ninja install
popd


mkdir -p /tmp && pushd /tmp
curl -LO https://github.com/BurntSushi/ripgrep/releases/download/13.0.0/ripgrep_13.0.0_amd64.deb
dpkg -i ripgrep_13.0.0_amd64.deb
popd

# timezone
apt-get install -yq tzdata
ln -fs /usr/share/zoneinfo/Asia/Shanghai /etc/localtime && \
  dpkg-reconfigure -f noninteractive tzdata
zsh
sed -i 's/required/sufficient/g' /etc/pam.d/chsh
chsh -s /usr/bin/zsh
}
function install_tmux(){

# tmux
sed -i 's/archive.ubuntu.com/mirrors.ustc.edu.cn/g' /etc/apt/sources.list
sed -i 's/cn.archive.ubuntu.com/mirrors.ustc.edu.cn/g' /etc/apt/sources.list
sed -i 's/security.ubuntu.com/mirrors.ustc.edu.cn/g' /etc/apt/sources.list
pip3 config set global.index-url https://pypi.tuna.tsinghua.edu.cn/simple

apt-get update

mkdir -p /${OPT_DIR} && pushd /${OPT_DIR}
apt remove -y tmux
apt install -y libevent-dev ncurses-dev build-essential bison pkg-config
rm -rf  tmux-3.3a.tar.gz
wget https://github.com/tmux/tmux/releases/download/3.3a/tmux-3.3a.tar.gz
tar -zxf tmux-3.3a.tar.gz
cd tmux-3.3a
./configure
make -j60 && make install
popd
}
  

function init_dir(){
  if [[ "" != $2 ]]; then
    OPT_DIR="$2"
    echo "all applications will be installed to ${OPT_DIR}"
  fi
  mkdir -p ${OPT_DIR}
  mkdir -p ~/.config
}

if [[ "run" == $1 ]]; then
  run
elif [[ "exec" == $1 ]]; then
  exec
elif [[ "install" == $1 ]]; then
  init_dir
  install
elif [[ "tmux" == $1 ]]; then
  init_dir
  install_tmux
elif [[ "nvim" == $1 ]]; then
  add-apt-repository -y ppa:neovim-ppa/unstable
  apt-get install -y neovim
fi
