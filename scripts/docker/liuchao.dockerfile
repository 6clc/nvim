ARG BASE_IMAGE
FROM ${BASE_IMAGE}

USER root
SHELL ["/bin/bash", "-c"]

# ARG NODE_VERSION=17.8.0
# RUN curl https://nodejs.org/dist/v$NODE_VERSION/node-v$NODE_VERSION-linux-x64.tar.gz | tar -xz -C /usr/local --strip-components 1


ARG USERNAME
ARG USER_UID
ARG USER_GID
RUN apt-get update
RUN groupadd -r -f -g ${USER_GID} ${USERNAME} && \
    useradd -o -r -l -u ${USER_UID} -g ${USER_GID} -ms /bin/bash ${USERNAME} || true
RUN apt-get install -y sudo tmux wget && usermod -aG sudo ${USERNAME}
RUN echo "${USERNAME}:0000" | chpasswd


USER $USERNAME


# CMD ["zsh"]
