docker rm -f easyconnect_cam

docker run \
  --name easyconnect_cam \
  --device /dev/net/tun --cap-add NET_ADMIN \
  -tid -e PASSWORD=xxxx -v $HOME/.ecdata:/root \
  -p 127.0.0.1:5901:5901 \
  -p 127.0.0.1:1080:1080 \
  -p 127.0.0.1:8888:8888 hagb/docker-easyconnect:7.6.7
