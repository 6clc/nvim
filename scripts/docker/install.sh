OPT_DIR=/tmp
OPT_DIR=~/opt

TMUX_PATH=https://github.com/tmux/tmux/releases/download/3.2a/tmux-3.2a.tar.gz
NVIM_PATH=""



function init_dir(){
  if [[ "" != $2 ]]; then
    OPT_DIR="$2"
    echo "all applications will be installed to ${OPT_DIR}"
  fi
  mkdir -p ${OPT_DIR}
  mkdir -p ~/.config
}

function install_ubuntu_pre(){
  sed -i 's/archive.ubuntu.com/mirrors.ustc.edu.cn/g' /etc/apt/sources.list
  sed -i 's/cn.archive.ubuntu.com/mirrors.ustc.edu.cn/g' /etc/apt/sources.list
  sed -i 's/security.ubuntu.com/mirrors.ustc.edu.cn/g' /etc/apt/sources.list
  pip3 config set global.index-url https://pypi.tuna.tsinghua.edu.cn/simple
  add-apt-repository -y ppa:neovim-ppa/unstable
  add-apt-repository -y ppa:git-core/ppa
  apt-get update
  apt install -y libevent-dev ncurses-dev build-essential bison pkg-config
  apt remove -y neovim
  apt install -y ninja-build gettext libtool-bin cmake g++  unzip curl
  install_tmux

}

function install_centos_pre(){
  yum install libevent libevent-devel
  yum install ncurses-devel
  install_tmux
}


# compile install tmux
function install_tmux(){
  mkdir -p /${OPT_DIR} && pushd /${OPT_DIR}
  apt remove -y tmux
  rm -rf  tmux-3.2a.tar.gz
  wget ${TMUX_PATH}
  tar -zxf tmux-3.2a.tar.gz
  cd tmux-3.2a
  ./configure
  make -j60 && make install
  popd

}

# compile install neovim
function build_install_neovim(){
  # neovim
  mkdir -p /${OPT_DIR} && pushd /${OPT_DIR}
  # rm -rf neovim
  git clone https://github.com/neovim/neovim.git
  cd neovim
  # git reset --hard 204a8b17c
  make -j60 CMAKE_BUILD_TYPE=RelWithDebInfo
  make install
  popd
}

function install_neovim(){
  mkdir -p /${OPT_DIR} && pushd /${OPT_DIR}
  if [[ ! -f  nvim-linux64.tar.gz ]]; then
    wget ${NVIM_PATH}
    tar -zxvf nvim-linux64.tar.gz
  fi
  echo "export PATH=${OPT_DIR}/nvim-linux64/bin/:\${PATH}" >>  ~/.bashrc
  echo "export PATH=${OPT_DIR}/nvim-linux64/bin/:\${PATH}" >>  ~/.zshrc

  if [[ ! -f ripgrep-14.1.0-x86_64-unknown-linux-musl.tar.gz ]]; then
    wget ${GREP_PATH}
    tar -zxvf  ripgrep-14.1.0-x86_64-unknown-linux-musl.tar.gz
  fi
  echo "export PATH=${OPT_DIR}/ripgrep-14.1.0-x86_64-unknown-linux-musl/:\${PATH}" >>  ~/.bashrc
  echo "export PATH=${OPT_DIR}/ripgrep-14.1.0-x86_64-unknown-linux-musl/:\${PATH}" >>  ~/.zshrc
  popd
}

# install all of all except tmux in ubuntu
function install_ubuntu(){

  apt-get install -y zsh git

  install_neovim
  
  # ripgrep
  mkdir -p /tmp && pushd /tmp
  curl -LO https://github.com/BurntSushi/ripgrep/releases/download/13.0.0/ripgrep_13.0.0_amd64.deb
  dpkg -i ripgrep_13.0.0_amd64.deb
  popd

  # timezone
  apt-get install -yq tzdata
  ln -fs /usr/share/zoneinfo/Asia/Shanghai /etc/localtime && \
         dpkg-reconfigure -f noninteractive tzdata

  # zsh
  zsh
  sed -i 's/required/sufficient/g' /etc/pam.d/chsh
  chsh -s /usr/bin/zsh
}


# install all of all except tmux in centos
function install_centos(){
  install_neovim
  yum install zsh
  chsh -s /usr/bin/zsh
}


init_dir

if [[ "tmux" == $1 ]]; then
  if command -v apt-get ; then
    install_ubuntu_pre
  elif command -v yum ; then
    install_centos_pre
  else
    echo "system error , only support 'yum' and 'apt'"
    exit -1
  fi
else
  if command -v apt-get ; then
    install_ubuntu
  elif command -v yum ; then
    install_centos
  else
    echo "system error , only support yum and apt"
    exit -1
  fi
fi

